module utility.git;

import std.process;
import std.file;

import packaged.Package;
import std.typecons;

static class Git {
    static Tuple!(bool, "status", string, "path") clone(string name, string path, bool recursive) {
        assert(executeShell("git").status == 1, "Git is not installed on your system");

        import std.stdio : writeln;

        writeln("Cloning ", path);
        import std.string : format, removechars;
        import std.file : tempDir;

        string tempPath = format("%s/%s/", tempDir(), removechars(name, " "));
        if (tempPath.exists)
            tempPath.rmdirRecurse;
        auto proc = executeShell(format("git clone %s %s %s", recursive
                ? "--recursive" : "", path, tempPath));
        return typeof(return)(proc.status == 0, format("%s/%s/", tempDir(),
                removechars(name, " ")));
    }
}
