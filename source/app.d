import std.getopt;
import packaged.Package;
import packaged.Version;

int main(string[] args) {
    string searchPackage;
    string infoPackage;

    auto helpInformation = getopt(args, "S|Search", "Search and Install Package",
            &searchPackage, "i|info", "Information fo specific package", &infoPackage);

    if (args.length > 2 || helpInformation.helpWanted) {
        defaultGetoptPrinter("packageD - Copyright © 2018, Niklas Stambor",
                helpInformation.options);
    }

    if (searchPackage != null) {
        Package.searchAndInstall(searchPackage);
    }

    if (infoPackage) {
        Package.info(infoPackage);
    }
    return 1;
}
